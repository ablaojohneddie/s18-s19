//[TASKS]

//1. Create a class based constructor to define a blueprint for the character/player/hero object.

	class hero {
		constructor(name, alias, level) {
			this.heroName = name,
			this.heroAlias = alias,
			this.heroLevel = level,
			this.heroAttack = 5 * level;
			this.heroHealth = 2 ** level;

			this.faint = (target) => {
				if (target.health < 0) {
					console.log(`${target.alias} has faited`);
				}
			}
			this.punch = (target) => {
				console.log(`${this.alias} tackled ${target.alias}`);
				target.health = target.health - this.attack;
				console.log(`${target.alias} health is reduced to ${target.health}`)
				this.faint(target)
				
			}
		}
	}

	let hero1 = new hero('Joe', 'moJoe', 7);
	let hero2 = new hero('Angela', 'LangLang', 4);
	let hero3 = new hero('Mike', 'Mik', 5);
	let hero4 = new hero('Kimmy', 'Kim', 8);
	let hero5 = new hero('Tony', 'Tony', 6);
	let heroes = [hero1, hero2, hero3, hero4, hero5];
	console.table(hero);
	hero5.punch(hero4);

	let displayAll = () => console.table(heroes);
	let displayOne = () => console.table(heroes);
	displayAll(hero);
	displayOne(hero3);
	displayOne();


//2. Create multiple instances of objects using the 'constructor' atlest 5 objects.

//3. Display all characters

//4. Display a single characters status